import React, { Component } from "react";
import { Graph, DefaultLink } from "@visx/network";
import { withTooltip, TooltipWithBounds } from "@visx/tooltip";
import { WithTooltipProvidedProps } from "@visx/tooltip/lib/enhancers/withTooltip";

interface INode {
  x: number;
  y: number;
  colors: {
    fill: string;
    stroke: string;
  };
}

const nodes: INode[] = [
  //first group
  {
    x: 82,
    y: 100,
    colors: {
      fill: "#8983AB",
      stroke: "#444088",
    },
  },
  {
    x: 82,
    y: 160,
    colors: {
      fill: "#8983AB",
      stroke: "#444088",
    },
  },
  {
    x: 332,
    y: 100,
    colors: {
      fill: "#8983AB",
      stroke: "#444088",
    },
  },
  // second group
  {
    x: 82,
    y: 300,
    colors: {
      fill: "#C785A1",
      stroke: "#BA006F",
    },
  },
  {
    x: 282,
    y: 360,
    colors: {
      fill: "#C785A1",
      stroke: "#BA006F",
    },
  },
  {
    x: 282,
    y: 300,
    colors: {
      fill: "#C785A1",
      stroke: "#BA006F",
    },
  },
  {
    x: 432,
    y: 300,
    colors: {
      fill: "#C785A1",
      stroke: "#BA006F",
    },
  },
  // third group
  {
    x: 82,
    y: 516,
    colors: {
      fill: "#70AAAA",
      stroke: "#00898A",
    },
  },
  {
    x: 282,
    y: 576,
    colors: {
      fill: "#70AAAA",
      stroke: "#00898A",
    },
  },
  {
    x: 282,
    y: 516,
    colors: {
      fill: "#70AAAA",
      stroke: "#00898A",
    },
  },
  {
    x: 432,
    y: 516,
    colors: {
      fill: "#70AAAA",
      stroke: "#00898A",
    },
  },
  {
    x: 632,
    y: 516,
    colors: {
      fill: "#70AAAA",
      stroke: "#00898A",
    },
  },
  {
    x: 432,
    y: 460,
    colors: {
      fill: "#70AAAA",
      stroke: "#00898A",
    },
  },
  // outside
  {
    x: 82,
    y: 650,
    colors: {
      fill: "#70AAAA",
      stroke: "#00898A",
    },
  },
];

const dataSample = {
  nodes,
  links: [
    //first group
    { source: nodes[0], target: nodes[1] },
    { source: nodes[2], target: nodes[0] },
    //second group
    { source: nodes[3], target: nodes[5] },
    { source: nodes[4], target: nodes[5] },
    { source: nodes[5], target: nodes[6] },
    //third group
    { source: nodes[7], target: nodes[9] },
    { source: nodes[9], target: nodes[8] },
    { source: nodes[9], target: nodes[10] },
    { source: nodes[10], target: nodes[11] },
    { source: nodes[10], target: nodes[12] },
    //additional
    { source: nodes[6], target: nodes[12] },
    { source: nodes[13], target: nodes[7] },
  ],
};

const width = "800px";
const height = "676px";

interface IProps {
  node: INode;
  toggleTooltip: (params: ITooltipToggleParams) => void;
}

class CustomNode extends Component<IProps> {
  render = () => {
    const { node, toggleTooltip } = this.props;

    return (
      <rect
        cursor={"pointer"}
        onClick={() => {
          toggleTooltip({
            tooltipLeft: node.x,
            tooltipTop: node.y,
            tooltipData: "Test",
          });
        }}
        transform={"translate(-24, -24)"}
        width={48}
        height={48}
        rx={5}
        dx={-24}
        fill={node.colors.fill}
        stroke={node.colors.stroke}
        strokeWidth={2}
      />
    );
  };
}

interface ITooltipToggleParams {
  tooltipLeft: number;
  tooltipTop: number;
  tooltipData: string;
}

class NetworkGraph extends Component<WithTooltipProvidedProps<string>> {
  toggleTooltip = (params: ITooltipToggleParams) => {
    const { hideTooltip, showTooltip, tooltipOpen } = this.props;

    if (tooltipOpen) {
      hideTooltip();
    } else {
      showTooltip(params);
    }
  };

  render = () => {
    const { tooltipData, tooltipLeft, tooltipTop, tooltipOpen } = this.props;

    return (
      <>
        <svg id="network-graph" width={width} height={height}>
          <rect width={32} height={200} fill={"#8983AB"} />
          <rect x={32} width={768} height={200} fill={"#E9E8F1"} />
          <text
            textAnchor={"end"}
            transform={"rotate(-90)"}
            y={22}
            x={-32}
            fill={"white"}
            fontWeight={"bold"}
            fontSize={16}
          >
            Electricity Domain
          </text>

          <rect y={208} width={32} height={200} fill={"#C785A1"} />
          <rect x={32} y={208} width={768} height={200} fill={"#F9EDF2"} />
          <text
            textAnchor={"end"}
            transform={"rotate(-90)"}
            y={22}
            x={-240}
            fill={"white"}
            fontWeight={"bold"}
            fontSize={16}
          >
            Heating Domain
          </text>

          <rect y={416} width={32} height={200} fill={"#70AAAA"} />
          <rect x={32} y={416} width={768} height={200} fill={"#E9F3F4"} />
          <text
            textAnchor={"end"}
            transform={"rotate(-90)"}
            y={22}
            x={-448}
            fill={"white"}
            fontWeight={"bold"}
            fontSize={16}
          >
            Cooling Domain
          </text>

          <Graph
            graph={dataSample}
            linkComponent={DefaultLink}
            nodeComponent={(props) => (
              <CustomNode toggleTooltip={this.toggleTooltip} {...props} />
            )}
          />
        </svg>
        {tooltipOpen && (
          <TooltipWithBounds
            key={Math.random()}
            top={tooltipTop}
            left={tooltipLeft}
          >
            <div style={{ backgroundColor: "white", maxWidth: "200px" }}>
              <h3>{tooltipData}</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                mollis mollis mi ut ultricies. Nullam magna ipsum, porta vel dui
                convallis, rutrum imperdiet eros. Aliquam erat volutpat.
              </p>
            </div>
          </TooltipWithBounds>
        )}
      </>
    );
  };
}

export default withTooltip<{}, string>(NetworkGraph, {
  style: { position: "relative" },
});
