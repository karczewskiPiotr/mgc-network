import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface IMarker {
  position: L.LatLngTuple;
}

export interface IPolyline {
  positions: L.LatLngTuple[];
}

interface IMapState {
  markers: IMarker[];
  polylines: IPolyline[];
  mousePosition: L.LatLngTuple | null;
}

const initialState: IMapState = {
  markers: [
    {
      position: [51.505, -0.09],
    },
    {
      position: [51.508, -0.12],
    },
  ],
  polylines: [
    {
      positions: [
        [51.505, -0.09],
        [51.508, -0.12],
      ],
    },
  ],
  mousePosition: null,
};

export const mapSlice = createSlice({
  name: "map",
  initialState,
  reducers: {
    setMarkers: (state, action: PayloadAction<IMarker[]>) => {
      state.markers = action.payload;
    },
    setPolylines: (state, action: PayloadAction<IPolyline[]>) => {
      state.polylines = action.payload;
    },
    setMousePosition: (state, action: PayloadAction<L.LatLngTuple>) => {
      state.mousePosition = action.payload;
    },
  },
});

export const { setMarkers, setPolylines, setMousePosition } = mapSlice.actions;

export default mapSlice.reducer;
