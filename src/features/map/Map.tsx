import React, { Component } from "react";
import { MapContainer, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import Markers from "./components/Markers/Markers";
import Polylines from "./components/Polylines/Polylines";
import { MapContext } from "./MapContext";

export default class Map extends Component {
  static contextType = MapContext;
  context!: React.ContextType<typeof MapContext>;

  render = () => {
    const { setMap } = this.context;

    return (
      <MapContainer
        center={[51.505, -0.09]}
        zoom={13}
        scrollWheelZoom={true}
        style={{ width: "80vw", height: "80vh" }}
        whenCreated={setMap}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
          url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
        />
        <Markers />
        <Polylines />
      </MapContainer>
    );
  };
}
