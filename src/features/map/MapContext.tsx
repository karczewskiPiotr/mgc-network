import { Map } from "leaflet";
import React, { Component, createContext } from "react";

export interface IMapProviderState {
  map: null | Map;
  setMap: (map: Map) => void;
}

export const MapContext = createContext<IMapProviderState>({
  map: null,
  setMap: (_map: Map) => {},
});

export class MapProvider extends Component<{}, IMapProviderState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      map: null,
      setMap: this.setMap,
    };
  }

  setMap = (map: Map) => {
    this.setState({ map });
  };

  render = () => {
    const value = this.state;

    return <MapContext.Provider value={value} {...this.props} />;
  };
}
