import React, { Component } from "react";
import { Marker, MarkerProps } from "react-leaflet";
import HubIcon from "./hub-marker.svg";
import L from "leaflet";

export default class HubMarker extends Component<MarkerProps> {
  render = () => {
    const { children } = this.props;

    const icon = L.icon({
      iconUrl: HubIcon,
      iconRetinaUrl: HubIcon,
      iconSize: [64, 64],
      popupAnchor: [0, -26],
    });

    return (
      <Marker {...this.props} icon={icon}>
        {children}
      </Marker>
    );
  };
}
