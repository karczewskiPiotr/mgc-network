import React, { Component } from "react";
import { Popup } from "react-leaflet";
import { connect } from "react-redux";
import { RootState } from "../../../../app/store";
import { IMarker } from "../../mapSlice";
import HubMarker from "../HubMarker/HubMarker";

interface IStateProps {
  markers: IMarker[];
}

class Markers extends Component<IStateProps> {
  render = () => {
    const { markers } = this.props;

    return (
      <>
        {markers.map((marker, index) => (
          <HubMarker key={index} position={marker.position}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </HubMarker>
        ))}
      </>
    );
  };
}

const mapStateToProps = (state: RootState) => ({
  markers: state.map.markers,
});

export default connect(mapStateToProps)(Markers);
