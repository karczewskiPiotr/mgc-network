import React, { Component } from "react";
import { Polyline, PolylineProps } from "react-leaflet";

export default class BranchPolyline extends Component<PolylineProps> {
  render = () => {
    const { children } = this.props;

    return (
      <Polyline {...this.props} color={"#1A4270"}>
        {children}
      </Polyline>
    );
  };
}
