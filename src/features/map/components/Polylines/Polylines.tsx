import React, { Component } from "react";
import { Popup } from "react-leaflet";
import { connect } from "react-redux";
import { RootState } from "../../../../app/store";
import { IPolyline } from "../../mapSlice";
import BranchPolyline from "../BranchPolyline/BranchPolyline";

interface IStateProps {
  polylines: IPolyline[];
}

class Polylines extends Component<IStateProps> {
  render = () => {
    const { polylines } = this.props;

    return (
      <>
        {polylines.map((polyline, index) => (
          <BranchPolyline key={index} positions={polyline.positions}>
            <Popup>
              Popup on Branch. <br /> Easily customizable.
              <div
                style={{ cursor: "pointer" }}
                onClick={() => alert("Click in popup happened")}
              >
                Click me!
              </div>
            </Popup>
          </BranchPolyline>
        ))}
      </>
    );
  };
}

const mapStateToProps = (state: RootState) => ({
  polylines: state.map.polylines,
});

export default connect(mapStateToProps)(Polylines);
