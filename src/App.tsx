import React, { Component } from "react";
import MapControls from "./components/MapControls/MapControls";
import Nav from "./components/Nav/Nav";
import Map from "./features/map/Map";
import { MapProvider } from "./features/map/MapContext";
import NetworkGraph from "./features/network/NetworkGraph";

export enum VIEW_TYPES {
  MAP,
  NETWORK,
}

export interface IState {
  view: VIEW_TYPES;
}

export default class App extends Component<{}, IState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      view: VIEW_TYPES.MAP,
    };
  }

  renderView = () => {
    const { view } = this.state;

    switch (view) {
      case VIEW_TYPES.MAP:
        return (
          <MapProvider>
            <Map />
            <MapControls />
          </MapProvider>
        );
      case VIEW_TYPES.NETWORK:
        return <NetworkGraph />;
      default:
        return <></>;
    }
  };

  setView = (view: VIEW_TYPES) => this.setState({ view });

  render = () => {
    return (
      <div
        className="app"
        style={{
          width: "100vw",
          height: "100vh",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Nav setView={this.setView} />
        {this.renderView()}
      </div>
    );
  };
}
