import { Dispatch } from "@reduxjs/toolkit";
import L, { LeafletMouseEvent } from "leaflet";
import React, { Component } from "react";
import { connect } from "react-redux";
import { RootState } from "../../app/store";
import { MapContext } from "../../features/map/MapContext";
import { IMarker, setMousePosition } from "../../features/map/mapSlice";

interface IStateProps {
  markers: IMarker[];
  mousePosition: L.LatLngTuple | null;
}

interface IDispatchProps {
  onMouseClick: (position: L.LatLngTuple) => void;
}

class MapControls extends Component<IStateProps & IDispatchProps> {
  static contextType = MapContext;
  context!: React.ContextType<typeof MapContext>;

  configureMapEvents = () => {
    const { map } = this.context;
    const { onMouseClick } = this.props;

    if (map === null) return;

    map.removeEventListener("click");

    map.on("click", (e: LeafletMouseEvent) => {
      const position: L.LatLngTuple = [e.latlng.lat, e.latlng.lat];
      console.log("really?");

      onMouseClick(position);
    });
  };

  componentDidMount = () => {
    this.configureMapEvents();
  };

  componentDidUpdate = () => {
    this.configureMapEvents();
  };

  render = () => {
    const { map } = this.context;
    const { markers, mousePosition } = this.props;

    if (map === null) return <></>;

    return (
      <div style={{ marginTop: "0.5rem" }}>
        <button
          onClick={() => {
            map.flyTo(markers[0].position, map.getMaxZoom());
          }}
        >
          Center on Hub
        </button>
        <button
          onClick={() => {
            map.fitBounds(
              L.latLngBounds(markers.map((marker) => marker.position))
            );
          }}
        >
          Fit bounds of system
        </button>
        <p>
          Mouse position:{" "}
          {mousePosition?.toLocaleString() ??
            "Click on the map to grab position."}
        </p>
      </div>
    );
  };
}

const mapStateToProps = (state: RootState) => ({
  markers: state.map.markers,
  mousePosition: state.map.mousePosition,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onMouseClick: (position: L.LatLngTuple) =>
    dispatch(setMousePosition(position)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MapControls);
