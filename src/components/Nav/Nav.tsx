import React, { Component } from "react";
import { VIEW_TYPES } from "../../App";

interface IProps {
  setView: (view: VIEW_TYPES) => void;
}

export default class Nav extends Component<IProps> {
  render = () => {
    const { setView } = this.props;

    return (
      <nav>
        <ul>
          <li
            style={{
              display: "inline-block",
              margin: "4px",
              cursor: "pointer",
            }}
            onClick={() => {
              setView(VIEW_TYPES.MAP);
            }}
          >
            Map
          </li>
          <li
            style={{
              display: "inline-block",
              margin: "4px",
              cursor: "pointer",
            }}
            onClick={() => {
              setView(VIEW_TYPES.NETWORK);
            }}
          >
            Network
          </li>
        </ul>
      </nav>
    );
  };
}
